package com.learning.restapis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.learning.restapis.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long>{

	User findByusername(String username);
	User findByemail(String email);
}
