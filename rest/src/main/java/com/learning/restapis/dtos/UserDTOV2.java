package com.learning.restapis.dtos;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonView;
import com.learning.restapis.entities.Order;
import com.learning.restapis.entities.Views;

public class UserDTOV2 {

	private String username;
	private String name;
	private String email;
	private String role;
	private String ssn;
	private String address;
	
	public UserDTOV2() {
		//empty
	}
	
	public UserDTOV2(String username, String name, String email, String role, String ssn,
			String address) {
		super();
		this.username = username;
		this.name = name;
		this.email = email;
		this.role = role;
		this.ssn = ssn;
		this.address = address;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	
}
