package com.learning.restapis;

import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.Locale;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

import springfox.documentation.swagger2.annotations.EnableSwagger2;


@SpringBootApplication
public class SpringBootApiBUildingBlocksApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootApiBUildingBlocksApplication.class, args);
	}

	@Bean
	public LocaleResolver localResolver() {
		AcceptHeaderLocaleResolver localResolver = new AcceptHeaderLocaleResolver();
		localResolver.setDefaultLocale(Locale.US);
		return localResolver;
	}
	
	public ResourceBundleMessageSource messageSource() {
		ResourceBundleMessageSource messageSources = new ResourceBundleMessageSource();
		messageSources.setBasename("messages");
		return messageSources;
	}
}
