package com.learning.restapis.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonView;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description="This model is to hold user and order Info")
@Entity
@Table(name = "users")
//@JsonIgnoreProperties({"ssn","name"})  //--static Filtering part
//@JsonFilter(value = "filter")	//--mapping jackson filtering section
public class User {
	
	//h2 console url=127.0.0.1:8080/h2-console
	//insert into users values(101,'email@email.com','name','admin','ssnnumber','user@uqi')
	@Id   //JPA idfy it's an primary key
	@GeneratedValue //auto generate value
	@JsonView(Views.Internal.class)
	@ApiModelProperty(notes = "Auto generated unique id", required = false, position = 1)
	private Long id;
	
	
//	 Add this 2 library for Spring Validation
//	  <dependency>
//    		<groupId>javax.validation</groupId>
//    		<artifactId>validation-api</artifactId>
//		</dependency>
//		
//		<dependency>
//  			<groupId>org.springframework.boot</groupId>
//  			<artifactId>spring-boot-starter-validation</artifactId>
//		</dependency>
	 
	@NotEmpty(message="User Name Should not empty")
	@Size(min=2, message="Username should have atleast 2 Chars")
	@Column(name= "user_name",length=50, nullable=false, unique=true)  //become column name in DB
	@JsonView(Views.External.class)
	private String username;
	
	@NotEmpty(message="Name is Required")
	@Size(min=2, message="Name should have atleast 2 Chars")
	@Column(name= "name",length=50, nullable=false)
	@JsonView(Views.External.class)
	@ApiModelProperty(notes = "User Name", required = true, position = 2)
	private String name;
	
	@ApiModelProperty(notes = "User Email", required = true, position = 3, example="example@example.com")
	@Column(name= "email",length=50, nullable=false)
	@JsonView(Views.External.class)
	private String email;
	
	@Column
	@JsonView(Views.Internal.class)
	@ApiModelProperty(notes = "User Role", required = true, position = 6)
	private String role;
	
	//@JsonIgnore  //--static Filtering part
	@Column(name= "ssn",length=50, nullable=false, unique=true)
	@JsonView(Views.Internal.class)
	@ApiModelProperty(notes = "SSN ID", required = true, position = 5)
	private String ssn;

	@OneToMany(mappedBy = "user") //named of variable in Order class
	@JsonView(Views.External.class)
	private List<Order> orders;
	
	@ApiModelProperty(notes = "User Address", required = true, position = 4)
	@Column(name= "address", length=150, nullable=true, unique=false)
	private String address;
	
	


	public User() {
		super();
	}



	public User(Long id, String username, String name, String email, String role, String ssn) {
		this.id = id;
		this.username = username;
		this.name = name;
		this.email = email;
		this.role = role;
		this.ssn = ssn;
		this.address = "";
	}

	public User(Long id,
			@NotEmpty(message = "User Name Should not empty") @Size(min = 2, message = "Username should have atleast 2 Chars") String username,
			@NotEmpty(message = "Name is Required") @Size(min = 2, message = "Name should have atleast 2 Chars") String name,
			String email, String role, String ssn, List<Order> orders, String address) {
		super();
		this.id = id;
		this.username = username;
		this.name = name;
		this.email = email;
		this.role = role;
		this.ssn = ssn;
		this.orders = orders;
		this.address = address;
	}

	public Long getId() {
		return id;
	}
	public String getUsername() {
		return username;
	}
	public String getName() {
		return name;
	}
	public String getEmail() {
		return email;
	}
	public String getRole() {
		return role;
	}
	public String getSsn() {
		return ssn;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public List<Order> getOrders() {
		return orders;
	}



	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}



	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + ", name=" + name + ", email=" + email + ", role=" + role
				+ ", ssn=" + ssn + ", address=" + address + "]";
	}


	
	
}
