package com.learning.restapis.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name="orders")
public class Order {

	@Id	//for act as primary key
	@GeneratedValue //for auto increment primary value
	@JsonView(Views.Internal.class)
	private Long orderId;
	@JsonView(Views.External.class)
	@Column(name= "order_description", nullable=false)
	private String orderDescription;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JsonIgnore  //ignore when sending during response
	private User user;

	public Order() {
		super();
	}

	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}

	public String getOrderDescription() {
		return orderDescription;
	}

	public void setOrderDescription(String orderDescription) {
		this.orderDescription = orderDescription;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	@Override
	public String toString() {
		return "Order [id=" + orderId + ", name=" + user.getName() + ", email=" + user.getEmail() + ", role=" + user.getRole()
				+ ", ssn=" + user.getSsn() + "]";
	}
	
}
