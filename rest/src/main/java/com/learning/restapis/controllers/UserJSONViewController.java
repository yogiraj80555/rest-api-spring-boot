package com.learning.restapis.controllers;

import java.util.Optional;

import javax.validation.constraints.Min;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.fasterxml.jackson.annotation.JsonView;
import com.learning.restapis.entities.User;
import com.learning.restapis.entities.Views;
import com.learning.restapis.exceptions.UserNotFoundException;
import com.learning.restapis.services.UserServices;

@RestController
@RequestMapping(value="/filter/users")
public class UserJSONViewController {

	
	@Autowired
	private UserServices userServices;
	
	@JsonView(Views.External.class)
	@GetMapping("/external-jsonview/{id}")
	public Optional<User> getUserIdExternalView(@PathVariable("id") @Min(1) Long id) {
		try {
			return userServices.getUser(id);
		} catch (UserNotFoundException e) {
			throw new ResponseStatusException(HttpStatus.BAD_GATEWAY,e.getMessage());
		}
		//return null;
	}
	
	@JsonView(Views.Internal.class)
	@GetMapping("/internal-jsonview/{id}")
	public Optional<User> getUserIdInternalView(@PathVariable("id") @Min(1) Long id) {
		try {
			return userServices.getUser(id);
		} catch (UserNotFoundException e) {
			throw new ResponseStatusException(HttpStatus.BAD_GATEWAY,e.getMessage());
		}
		//return null;
	}
}
