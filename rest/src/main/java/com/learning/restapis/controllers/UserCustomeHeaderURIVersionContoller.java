package com.learning.restapis.controllers;

import java.util.Optional;

import javax.validation.constraints.Min;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.learning.restapis.dtos.UserDTOV1;
import com.learning.restapis.dtos.UserDTOV2;
import com.learning.restapis.dtos.UserModelMapperDTO;
import com.learning.restapis.entities.User;
import com.learning.restapis.exceptions.UserNotFoundException;
import com.learning.restapis.services.UserServices;

@RestController
@RequestMapping(value = "/versioning/header/users/")
public class UserCustomeHeaderURIVersionContoller {

	@Autowired
	private UserServices userServices;

	@Autowired
	private ModelMapper modelMapper;

	// Request Header based Versioning -V1
	@GetMapping(value = "/{id}")
	public UserDTOV1 getUserIdV1(@PathVariable("id") @Min(1) Long id) throws UserNotFoundException {

		Optional<User> optionalUser = userServices.getUser(id);

		if (!optionalUser.isPresent()) {
			throw new UserNotFoundException("User Not Found");
		}
		User user = optionalUser.get();
		UserDTOV1 userDTOV1 = modelMapper.map(user, UserDTOV1.class);

		return userDTOV1;
	}

	// Request Header based Versioning -V2
	@GetMapping(value = "/{id}", headers = "")
	public UserDTOV2 getUserIdV2(@PathVariable("id") @Min(1) Long id) throws UserNotFoundException {

		Optional<User> optionalUser = userServices.getUser(id);

		if (!optionalUser.isPresent()) {
			throw new UserNotFoundException("User Not Found");
		}
		User user = optionalUser.get();
		UserDTOV2 userDTOV2 = modelMapper.map(user, UserDTOV2.class);

		return userDTOV2;
	}
}
