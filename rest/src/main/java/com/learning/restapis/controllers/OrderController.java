package com.learning.restapis.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.learning.restapis.entities.Order;
import com.learning.restapis.entities.User;
import com.learning.restapis.exceptions.OrderNotFoundException;
import com.learning.restapis.exceptions.UserNotFoundException;
import com.learning.restapis.repository.OrderRepository;
import com.learning.restapis.repository.UserRepository;

@RestController
@RequestMapping(value = "/users")
public class OrderController {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private OrderRepository orderRepository;
	
	@GetMapping("/{userid}/orders")
	public List<Order> getAllOrders(@PathVariable Long userid) throws UserNotFoundException {
		return isUserAvalible(userid).get().getOrders();
	}
	
	@GetMapping("/{userid}/orders/{orderid}")
	public Order getOrdersById(@PathVariable Long userid,@PathVariable Long orderid) {
		try {
		isUserAvalible(userid);
		Optional<Order> orders = orderRepository.findById(orderid);
		
		if (orders == null || orders.isEmpty()) {
			throw new OrderNotFoundException("Order Not Found");
		}
		return orders.get();
		}catch(Exception e) {
			throw new ResponseStatusException(HttpStatus.BAD_GATEWAY,e.getMessage());
		}
	}
	
	
	@PostMapping("/{userid}/orders")
	public boolean createOrder(@PathVariable Long userid, @RequestBody Order order) throws UserNotFoundException {
		Optional<User> userOptional = isUserAvalible(userid);	
		User user = userOptional.get();
		order.setUser(user);
		orderRepository.save(order);
		return true;
	}

	
	private Optional<User> isUserAvalible(Long userid) throws UserNotFoundException {
		Optional<User> userOptional = userRepository.findById(userid);
		if(!userOptional.isPresent()) {
			throw new UserNotFoundException("User "+userid+ " not Found");
		}
		return userOptional;
	}
}
