package com.learning.restapis.controllers;

import java.util.Optional;

import javax.validation.constraints.Min;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.learning.restapis.dtos.UserDTOV1;
import com.learning.restapis.dtos.UserDTOV2;
import com.learning.restapis.dtos.UserModelMapperDTO;
import com.learning.restapis.entities.User;
import com.learning.restapis.exceptions.UserNotFoundException;
import com.learning.restapis.services.UserServices;

@RestController
@RequestMapping(value="/versioning/uri/users/")
public class UserURIVersionContoller {

	
	@Autowired
	private UserServices userServices;
	
	@Autowired
	private ModelMapper modelMapper;
	
	//URI based Versioning -V1
	@GetMapping({"/v1.0/{id}","/v1.1/{id}"})
	public UserDTOV1 getUserIdV1(@PathVariable("id") @Min(1) Long id) throws UserNotFoundException {
		
		Optional<User> optionalUser = userServices.getUser(id);
		
		if(!optionalUser.isPresent()) {
			throw new UserNotFoundException("User Not Found");
		}
		User user = optionalUser.get();
		UserDTOV1 userDTOV1 = modelMapper.map(user, UserDTOV1.class);
	
		return userDTOV1;
	}
	
	//URI based Versioning -V2
	@GetMapping({"/v2.0/{id}","/v2.1/{id}"})
	public UserDTOV2 getUserIdV2(@PathVariable("id") @Min(1) Long id) throws UserNotFoundException {
		
		Optional<User> optionalUser = userServices.getUser(id);
		
		if(!optionalUser.isPresent()) {
			throw new UserNotFoundException("User Not Found");
		}
		User user = optionalUser.get();
		UserDTOV2 userDTOV2 = modelMapper.map(user, UserDTOV2.class);
	
		return userDTOV2;
	}
}
