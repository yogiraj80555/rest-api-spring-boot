package com.learning.restapis.controllers;

import java.util.Optional;

import javax.validation.constraints.Min;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.learning.restapis.dtos.UserModelMapperDTO;
import com.learning.restapis.entities.User;
import com.learning.restapis.exceptions.UserNotFoundException;
import com.learning.restapis.services.UserServices;

@RestController
@RequestMapping("modelmapper/users")
public class ModelMapperController {

	@Autowired
	private UserServices userServices;
	
	@Autowired
	private ModelMapper modelMapper;
	
	
	@GetMapping("/{id}")
	public UserModelMapperDTO getUserId(@PathVariable("id") @Min(1) Long id) throws UserNotFoundException {
		
		Optional<User> optionalUser = userServices.getUser(id);
		
		if(!optionalUser.isPresent()) {
			throw new UserNotFoundException("User Not Found");
		}
		User user = optionalUser.get();
		UserModelMapperDTO userModelMapperDto = modelMapper.map(user, UserModelMapperDTO.class);
	
		return userModelMapperDto;
	}
}

