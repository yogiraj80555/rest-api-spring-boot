package com.learning.restapis.controllers;

import java.util.Optional;

import javax.validation.constraints.Min;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.learning.restapis.dtos.UserDTOV1;
import com.learning.restapis.dtos.UserDTOV2;
import com.learning.restapis.dtos.UserModelMapperDTO;
import com.learning.restapis.entities.User;
import com.learning.restapis.exceptions.UserNotFoundException;
import com.learning.restapis.services.UserServices;

@RestController
@RequestMapping(value = "/versioning/media-type/users/")
public class UserMediaTypeURIVersionContoller {

	@Autowired
	private UserServices userServices;

	@Autowired
	private ModelMapper modelMapper;

	// Request Media type based Versioning -V1
	@GetMapping(value = "/{id}", produces = "application/vnd.yogiraj.app-v1+json")
	public UserDTOV1 getUserIdV1(@PathVariable("id") @Min(1) Long id) throws UserNotFoundException {

		Optional<User> optionalUser = userServices.getUser(id);

		if (!optionalUser.isPresent()) {
			throw new UserNotFoundException("User Not Found");
		}
		User user = optionalUser.get();
		UserDTOV1 userDTOV1 = modelMapper.map(user, UserDTOV1.class);

		return userDTOV1;
	}

	// Request Media type based Versioning -V2
	@GetMapping(value = "/{id}", produces = "application/vnd.yogiraj.app-v2+json")
	public UserDTOV2 getUserIdV2(@PathVariable("id") @Min(1) Long id) throws UserNotFoundException {

		Optional<User> optionalUser = userServices.getUser(id);

		if (!optionalUser.isPresent()) {
			throw new UserNotFoundException("User Not Found");
		}
		User user = optionalUser.get();
		UserDTOV2 userDTOV2 = modelMapper.map(user, UserDTOV2.class);

		return userDTOV2;
	}
}
