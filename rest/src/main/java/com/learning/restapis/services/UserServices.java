package com.learning.restapis.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import com.learning.restapis.entities.User;
import com.learning.restapis.exceptions.UserExistsException;
import com.learning.restapis.exceptions.UserNotFoundException;
import com.learning.restapis.repository.UserRepository;

@Service
public class UserServices {

	
	//autowire
	@Autowired
	private UserRepository userRepository;
	
	//get all users
	public List<User> getAllUsers(){
		return userRepository.findAll();
	}
	
	//create user
	public User createUser(User user)throws  UserExistsException{
		User preUser = userRepository.findByemail(user.getEmail());
		if(preUser != null) {
			throw new UserExistsException("User Already exists.");
		}
		
		return userRepository.save(user);
	}
	
	public Optional<User> getUser(Long id) throws UserNotFoundException {
		Optional<User> user = userRepository.findById(id);
		
		if(!user.isPresent()) {
			throw new UserNotFoundException("User Not Found");
		}
		 return user;
	}
	
	public User getUserByName(String name) {
		return userRepository.findByusername(name);
	}
	
	public User updateUserById(Long id, User user) throws UserNotFoundException  {
		Optional<User> oUser = userRepository.findById(id);
		
		if(!oUser.isPresent()) {
			throw new UserNotFoundException("User Not Found");
		}
		
		return userRepository.save(new User(id,user.getName(),user.getName() , user.getEmail(),user.getRole(),user.getSsn()));
	}
	
	
	public boolean deleteUserById(Long id) {
		if(!userRepository.findById(id).isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST,"User Not Found");
		}
		userRepository.deleteById(id);
		return true;
	}
}
