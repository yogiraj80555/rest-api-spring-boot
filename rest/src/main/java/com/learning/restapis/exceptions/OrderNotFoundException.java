package com.learning.restapis.exceptions;

public class OrderNotFoundException extends Exception {
	
	private static final long serialVersionUID = 10000;

	public OrderNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public OrderNotFoundException(String message) {
		super(message);
	}

	public OrderNotFoundException(Throwable cause) {
		super(cause);
	}
}
