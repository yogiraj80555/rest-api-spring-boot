package com.learning.restapis.exceptions;

import java.util.Date;

public class CustomErrorDetails {
	
	private final Date date;
	private final String message;
	private final String errerDetails;
	public CustomErrorDetails(Date date, String message, String errerDetails) {
		super();
		this.date = date;
		this.message = message;
		this.errerDetails = errerDetails;
	}
	
	//Getters
	public Date getDate() {
		return date;
	}
	public String getMessage() {
		return message;
	}
	public String getErrerDetails() {
		return errerDetails;
	}
	
	

}
