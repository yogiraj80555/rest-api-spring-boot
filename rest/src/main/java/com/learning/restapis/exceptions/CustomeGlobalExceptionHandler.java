package com.learning.restapis.exceptions;

import java.util.Date;

import javax.validation.ConstraintViolationException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

//using this, this handler is globally applicable to all Controller
@ControllerAdvice
public class CustomeGlobalExceptionHandler extends ResponseEntityExceptionHandler{

	
	
	//Method Argument Not Valid Exception
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		
		CustomErrorDetails errDetails = new CustomErrorDetails(new Date(), "From Method Argument Not Valid", ex.getMessage());
		
		//return super.handleMethodArgumentNotValid(ex, headers, status, request);
		return new ResponseEntity<>(errDetails,HttpStatus.BAD_REQUEST);
	}
	
	
	//Request Method Not Supported
	@Override
	protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(HttpRequestMethodNotSupportedException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		CustomErrorDetails errDetails = new CustomErrorDetails(new Date(), "From Method Argument Not Valid", ex.getMessage());
		return new ResponseEntity<>(errDetails,HttpStatus.METHOD_NOT_ALLOWED);
	}

	
	//Username Not Found
	@ExceptionHandler(UserNotFoundException.class)
	public final ResponseEntity<Object> handleUserNotFoundException(UserNotFoundException ex, WebRequest req) {
		CustomErrorDetails errDetails = new CustomErrorDetails(new Date(), req.getSessionId(), ex.getMessage());
		return new ResponseEntity<>(errDetails,HttpStatus.NOT_FOUND);
	}
	
	@ExceptionHandler(ConstraintViolationException.class)
	public final ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException ex, WebRequest req) {
		CustomErrorDetails errDetails = new CustomErrorDetails(new Date(), req.getRemoteUser(), ex.getMessage());
		return new ResponseEntity<>(errDetails,HttpStatus.EXPECTATION_FAILED);
	}
}
