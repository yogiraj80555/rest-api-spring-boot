package com.learning.restapis.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class) //will pull limitation from User and Order POJO
public class SwaggerConfig {

	@Bean
	public Docket productApi() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("com.learning.restapis.controllers"))
				.paths(PathSelectors.ant("/users/**"))
				.build()
				.apiInfo(getApiInfo());
	}
	//Swagger Metadata: http://192.168.0.100:8080/v2/api-docs
	//swagger UI URL: http://192.168.0.100:8080/swagger-ui.html
	
	private ApiInfo getApiInfo() {
		return new ApiInfoBuilder()
		.title("User and Order System")
		.description("This page lists all API's in System")
		.version("V2.0")
		.contact(new Contact("Yogiraj Patil", 
				"https://yogiraj80555.github.io/YogirajPortfolio/", 
				"yogiraj.218m0064@viit.ac.in"))
		.license("under MIT License")
		.build();
	}
}










